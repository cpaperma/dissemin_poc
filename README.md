# dissemin_poc

Ingestion in the database should be done by inserting documents into a `raw`
table. Suggestion of signature for `raw`:

~~~~~~~~~~~~~~~~~~~~~~
  Column   |    Type    |                    Modifiers                  
-----------+------------+--------------------------------------------------
 id        | integer    | not null default nextval('raw_id_seq'::regclass)
 doc       | text       | 
 source    | source_doc | 
 error     | text       | default ''::text
 record_id | integer    | default '-1'::integer
~~~~~~~~~~~~~~~~~~~~~~

With `source_doc` being a custom serial type:

~~~~~~~~~~~~~~~~~~~~~~~~~~~
CREATE TYPE source_doc AS ENUM('crossref', 'base', [...]);
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A full-text-search indices can be performed on the raw documents depending 
on the source type. Could be a possibility to improve search behaviour on a field.

## The Proof Of Concept schema 
The current proof of concept came with a simple schema not far from 
the actual implementation of dissemin. 
An adapdation to the correct table with the correct field should be 
performed to apply the POC in prod.

In the simplify version, we have a `paper_infos` table, 
`papers_record` table, `authors_tables`.


## Triggers schemas

Inserting a row in `RAW` trigger a function `raw_insert`. 
The declaration can be done with: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
CREATE TRIGGER _raw_insert_trigger 
BEFORE INSERT 
ON raw 
FOR EACH ROW EXECUTE PROCEDURE 
raw_insert()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Where the function `raw_insert` is user defined that 
will extract informations and insert everywhere in 
the correct order.

A suggestion of implementation (simplified version 
compared to the one in `schema/schema.sql`).
Remark that the order of insertion into table is important. 
We need first to get the paper and journal identifier before inserting
informations to `paper_records` table and then inserting 
the authors and the authors relations to the paper.

Insertions into the different table should are done by adequate 
function for readability but could all be performed by 
the `raw_insert` function.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CREATE FUNCTION public.raw_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	--- Use JSON type to get a cleaned version of the raw_document.
	paper JSON := (SELECT raw_clean(new.doc, new.source::text))::JSON; 
	--- Store the fingerprint once for all for readibility. 
	paper_fingerprint UUID:= MD5(paper->>'fingerprint')::uuid;
	--- Add the paper to the paper table and get its ID back.
	paper_identity INT;-- := add_paper(paper, paper_fingerprint);
	--- Journal identification can not be done directly since it has to be identified first.
	journal_identity INT;
	--- record id
	nrecord INT;
BEGIN
	--- If the python's script return an error json, we don't pursue.
	IF (paper::JSONB ? 'error') THEN
		NEW.error = paper;
	ELSE
	BEGIN
		--- If journal title is not null then add and/or return its id to journal_identity
		IF (paper->'journal'->>'title' IS NOT NULL) THEN
			SELECT add_journal(paper->'journal') INTO journal_identity;
		END IF;
		--- Check if the fingerprint already appears add if not and returns the id in each cases.
		--- The with statement is here to prevent to perform multiple access to the papers_info table. 
		WITH paper_exists AS (
				 SELECT id FROM papers_info WHERE papers_info.fingerprint = paper_fingerprint
			 ),
			 paper_insert AS (
			  INSERT INTO
				  papers_info (title, subtitle, pubdate, fingerprint)
			  VALUES
			  (
			  paper->>'title',
			  paper->>'subtitle',
			  TO_DATE(paper->>'pubdate','YYYY-MM-DD'),
			  paper_fingerprint
			  )
			  ON CONFLICT (fingerprint) DO NOTHING
			  RETURNING id
			)
			SELECT id into paper_identity
		FROM (SELECT * FROM paper_exists UNION SELECT * FROM paper_insert) s;
		--- Insert a new papers_records
		INSERT INTO papers_records
		(fingerprint, doi, splash_url, url, description,doc_type, keywords, pubdate,raw_id, paper_id, journal_id, volume, pages)
		VALUES
		(
		 paper_fingerprint,
		 paper->>'doi',
		 paper->>'splash_url',
		 paper->>'pdf_url',
		 paper->>'description',
		 (paper->>'doc_type')::doctype,
		 keywords_int,
		 TO_DATE(paper->>'pubdate', 'YYYY-MM-DD'),
		 new.id,
		 paper_identity,
		 journal_identity,
		 paper->>'volume',
		 paper->>'pages'
		 )
		RETURNING id INTO nrecord_id;
		new.record_id := nrecord_id;
		--- INSERT authors in author table with the record_id.
		PERFORM insert_authors(paper->'authors', nrecord_id);
		END;
	END IF;
RETURN NEW;
END;
$$;

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Invoking python script. 

In order to make the treatment of raw documents easier, the 
best is to invoke a Python script that clean and extract 
a common data format for all different sources. 

It is possible to ingest raw document that have this format 
only, but it could lead to loose some informations in the process 
that could be of some use later. 

The cost of integrating a Python script to postgresql is rather 
low. To allow in the database `python3` as a language: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Then, the `raw_clean` function used in the insert can be of the following shape:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CREATE FUNCTION public.raw_clean(doc text, source text) RETURNS text
    LANGUAGE plpython3u
    AS $$

  import sys, json
  sys.path.append("/path/to/python/script")
  from custom_module import parse_info
  try:
  	return parse_info(doc, source)
  except Exception as E:
        return json.dumps({'error':str(E), 'type':str(type(E))})
$$;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Note, that the loading of the module is done once for each connection at most. 
It means that a change in the filesystem to the python script will have 
immediate repercussion on the execution of functions (no need to interrupt 
or restart the database). 

In particular, adding extra source requires only to change the update the `parse_info` 
function in Python to take into account the new raw document format. 


