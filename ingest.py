import psycopg2
import bz2
def ingest_crossref(db, file_path, batch_size=1000, verbose=False):
    g = bz2.open(file_path)
    c = db.cursor()
    i = 0
    while True:
        T = [(g.readline().decode(),) for _ in range(batch_size)]
        c.executemany("INSERT INTO raw (doc, source, record_id) VALUES (%s,'crossref',0)",T)
        db.commit()
        i += 1
        if verbose:
            print("loop", i, end="\r")
        if ("",) in T:
            break

if __name__ == "__main__":
    file_path = '/tmp/sample_crossref.json.bz2'
    db = psycopg2.connect(user="cha", database="dissemin_dyn_poc")
    ingest_crossref(db, file_path, verbose=True)
    
    

