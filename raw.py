import json
from dissemin_python_postgres.fingerprint import create_paper_plain_fingerprint
from dissemin_python_postgres.name import parse_comma_name, normalize_name_words
from dissemin_python_postgres.utils import date_from_dateparts

keys_definition = ["title","doi", "subtitle","visibility","pubdate","pdf_url","splash_url","doc_type", "keywords","journal","volume","pages","description"]
doctypes = ["thesis","report","proceedings-article","dataset","book-chapter","reference-entry","poster","journal-issue","preprint","journal-article","book","other"]
def pdf_crossref_url_parse(doc):
    try:
        t = doc.get("link", {})
        url_doc = next(filter(lambda e:'content-type' in e and ("pdf" in e['content-type']),
                              t))
        return url_doc['URL']
    except StopIteration:
        return ""
    
def authors_crossref_parse(doc):
    authors = doc.get("author", [])
    authors_list = []
    for dct in authors:
        result = None
        if 'family' in dct and 'given' in dct:
            result = (dct['given'], dct['family'])
        elif 'family' in dct:  # The 'Arvind' case
            result = ('', dct['family'])
        elif 'literal' in dct:
            result = parse_comma_name(dct['literal'])
        if result:
            result = (normalize_name_words(result[0]), normalize_name_words(result[1]))
            authors_list.append({'first_name':result[0],
                                 'last_name':result[1],
                                 'full_name': result[0]+' '+result[1],
                                 'orcid':dct.get('ORCID',None),
                                 'affiliation':dct.get('affiliation',None)
            })
    return authors_list

def identifiers_crossref_parse(doc):
    identifiers = []
    if "issn-type" in doc:
        for ident in doc["issn-type"]:
            if ident["type"] == "print":
                identifiers.append(("issn", ident["value"]))
            elif ident["type"] == "electronic":
                identifiers.append(("essn", ident["value"]))
            else:
                raise ValueError("Unknown identifiers Type {}".format(ident))
        if len(identifiers) > 0:
            return identifiers
    for key in ["ISSN", "ISBN", "ESSN"]:
        if key in doc:
            value = doc[key]
            if isinstance(value, list):
                value = value[0]
            return [(key.lower(), value)]
    return []

def parse_crossref(doc, text):
    meta = json.loads(text)
    for s in ['title', 'subtitle']:
        v = meta.get(s, None)
        if isinstance(v, list):
            v = v[0]
        doc[s] = v
    doc['doi'] = meta.get("DOI",None)
    date_str = meta.get("issued", None)
    if date_str is None:
        date_str = meta.get("created", None)
    if date_str is None:
        date_str = meta.get("deposited", None)
    if not date_str is None:
        date_str = date_from_dateparts(date_str["date-parts"][0])
        year = date_str.year
    doc["pubdate"] = str(date_str)
    doc["year"] = year
    doc["volume"] = meta.get("volume",None)
    doc["pages"] = meta.get("page", None)
    doc["doc_type"] = meta.get("type",None)
    doc["pdf_url"] = pdf_crossref_url_parse(meta)
    doc["splash_url"] = meta.get("URL",None)
    container = meta.get("container-title",None)
    if isinstance(container, list):
        container = container[0]
    doc["journal"]["title"] = container
    doc["journal"]["identifiers"] = identifiers_crossref_parse(meta)
    doc["journal"]["publisher"] = meta.get("publisher",None)
    doc["authors"] = authors_crossref_parse(meta)

def parse_base(doc, text):
    pass

def parse_info(text, source):
    """
    Parse text and return a JSON document with the string appearing in keys_definition
    global variable must appear here:
    to be there:
    * fingerprint -> (str) a normalized text representing the paper (for fingerprint). Is computed automatically here.
    * title       -> (str) the paper Title (Not Null)
    * doi         -> (str) The paper doi
    * subtitle    -> (str) Subtitle
    * visibility  -> (boolean)
    * pubdate     -> (str) Publication date in the format (YYYY-MM-DD format) Not Null
    * pdf_url     -> (str) An url for the pdf of the document
    * splash_url  -> (str) ..
    * doc_type    -> (str) the type of document. Value should be one of:
    thesis,report,proceedings-article,dataset,book-chapter,reference-entry,poster,journal-issue,preprint,journal-article,book,other
    * authors     -> list of (json):
            - full    -> (str)
            - last    -> (str)
            - first       -> (str)
            - orcid       -> (str)
            - affiliation -> (str)
    * keywords    -> list of (str)
    * journal     -> (json):
            - title       -> (str)
            - identifiers -> list of [(type, value)]
                       where type being ISSN, ISBN or ESSN
            - publisher   -> name (str)
    * volume      -> (str)
    * pages       -> (str)
    * description -> (str)
    Non existing value should be None.
    """
    doc = {}
    for key in keys_definition:
        doc.setdefault(key, None)
    doc["keywords"] = []
    doc["journal"] = {}
    doc["journal"].setdefault("title", None)
    doc["journal"].setdefault("publisher", None)
    doc["journal"].setdefault("identifiers", [])
    if source == 'crossref':
        parse_crossref(doc, text)
    elif source == 'base':
        parse_base(doc, text)
    else:
        raise NotImplementedError('{} is an unknown source'.format(source))
    if doc["title"] is None:
        raise ValueError("Document should have a title to be ingested")
    if doc["pubdate"] is None:
        raise ValueError("Document should have a pub date to be ingested")
    doc["fingerprint"] = create_paper_plain_fingerprint(doc["title"], doc["authors"], doc["year"])
    if doc["doc_type"] not in doctypes:
        raise ValueError("Not valid doc_type {}".format(doc["doc_type"]))
    return json.dumps(doc)
