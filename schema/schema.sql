--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


--
-- Name: doctype; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.doctype AS ENUM (
    'thesis',
    'report',
    'proceedings-article',
    'dataset',
    'book-chapter',
    'reference-entry',
    'poster',
    'journal-issue',
    'preprint',
    'journal-article',
    'book',
    'other'
);


ALTER TYPE public.doctype OWNER TO postgres;

--
-- Name: oa_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.oa_status AS ENUM (
    'unknow',
    'not accessible',
    'could be open',
    'open access'
);


ALTER TYPE public.oa_status OWNER TO postgres;

--
-- Name: publication_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.publication_status AS ENUM (
    'unkown',
    'restricted',
    'can',
    'cannot',
    'unclear'
);


ALTER TYPE public.publication_status OWNER TO postgres;

--
-- Name: serial_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.serial_type AS ENUM (
    'essn',
    'issn',
    'isbn'
);


ALTER TYPE public.serial_type OWNER TO postgres;

--
-- Name: serial_number; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.serial_number AS (
	serial text,
	type public.serial_type
);


ALTER TYPE public.serial_number OWNER TO postgres;

--
-- Name: source_doc; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.source_doc AS ENUM (
    'base',
    'crossref'
);


ALTER TYPE public.source_doc OWNER TO postgres;

--
-- Name: add_journal(json, integer); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.add_journal(journal_desc json, publisher_identity integer) RETURNS integer
    LANGUAGE sql
    AS $$
WITH
      journal_serial_numbers AS (
         SELECT ROW(a->>1, a->>0)::serial_number as serial
	 FROM
	     (SELECT json_array_elements(journal_desc->'identifiers') as a) s ),
      journal_exists AS (
         (SELECT j.journal_id as id
	 FROM
	 	journals_identifier as j,
		journal_serial_numbers as s
	 WHERE j.serial = s.serial)
	UNION
	(SELECT id
	FROM
		journals
	WHERE lower(journals.title) = lower(journal_desc->>'title')
	)),
      journal_insert AS (
         INSERT INTO journals
	 	(title, publisher_id)
	 SELECT
	 	 journal_desc->>'title',
	 	  publisher_identity
         WHERE
		NOT EXISTS (SELECT * FROM journal_exists)
	 RETURNING id),
      id_journal AS (
             (SELECT id FROM journal_insert) UNION (SELECT id FROM journal_exists)
      ),
      identifiers_insert AS (
      	 INSERT INTO journals_identifier
	 	SELECT j.id, s.serial
	 	FROM
		   id_journal j,
		   journal_serial_numbers s
	 ON CONFLICT DO NOTHING
      )
      SELECT id FROM id_journal;
$$;


ALTER FUNCTION public.add_journal(journal_desc json, publisher_identity integer) OWNER TO cha;

--
-- Name: add_journal_trgt(); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.add_journal_trgt() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO journals_identifier
SELECT new.id, s.serial
       FROM (SELECT UNNEST(new.identifiers) as serial) s;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.add_journal_trgt() OWNER TO cha;

--
-- Name: add_kw(text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.add_kw(kws text[]) RETURNS integer[]
    LANGUAGE sql
    AS $$
WITH
     kw_to_add AS (SELECT UNNEST(kws)as k), 
     old_kw AS (
          SELECT keywords.id, keywords.keyword FROM
     	  keywords,
     	  kw_to_add
     	  WHERE keywords.keyword = kw_to_add.k
     	  ),
     nkw AS (
     INSERT INTO keywords (keyword)
     SELECT * FROM kw_to_add
     ON CONFLICT (keyword) DO NOTHING
     RETURNING id
     )
     SELECT ARRAY_AGG(id)
     FROM (
          SELECT id FROM old_kw
     	  UNION
     	  SELECT id FROM nkw
	  ) a;
$$;


ALTER FUNCTION public.add_kw(kws text[]) OWNER TO postgres;

--
-- Name: add_paper(json, uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.add_paper(paper json, fingerprint uuid) RETURNS integer
    LANGUAGE sql
    AS $$
WITH paper_exists AS (
     SELECT id FROM papers_info WHERE papers_info.fingerprint = fingerprint
     ),
     paper_insert AS (
     INSERT INTO
     	    papers_info (title, subtitle, pubdate, fingerprint)
	    VALUES
		(
		paper->>'title',
		paper->>'subtitle',
		TO_DATE(paper->>'pubdate','YYYY-MM-DD'),
		fingerprint
		)
	    ON CONFLICT (fingerprint) DO NOTHING
	    RETURNING id)
     (SELECT * FROM paper_exists) UNION (SELECT * FROM paper_insert)

$$;


ALTER FUNCTION public.add_paper(paper json, fingerprint uuid) OWNER TO postgres;

--
-- Name: add_publisher(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.add_publisher(publisher_name text) RETURNS integer
    LANGUAGE sql
    AS $$
WITH publisher_exists as (
	SELECT id FROM publishers WHERE lower(name) = lower(publisher_name)
	),
     publisher_insert as (
	INSERT INTO publishers (name) VALUES  (publisher_name)
	ON CONFLICT DO NOTHING
	returning id)
(SELECT * FROM publisher_exists) UNION (SELECT * FROM publisher_insert);
$$;


ALTER FUNCTION public.add_publisher(publisher_name text) OWNER TO postgres;

--
-- Name: faceting_global_query(text[]); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.faceting_global_query(query text[]) RETURNS TABLE(doctype public.doctype, count integer)
    LANGUAGE sql
    AS $$

SELECT doc_type, #(ids & s.results_ids) as count
FROM _agg_records_doc_type,
     (SELECT ARRAY_AGG(results.paper_id) as results_ids
     FROM get_all_by_ids(query) as results)as s

$$;


ALTER FUNCTION public.faceting_global_query(query text[]) OWNER TO cha;

--
-- Name: get_all_by_ids(text[]); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.get_all_by_ids(args text[]) RETURNS TABLE(paper_id integer)
    LANGUAGE sql
    AS $$
SELECT record_id FROM raw 
WHERE
error='' and to_tsvector('english', doc) @@ to_tsquery('english', array_to_string(args, '&',''))
$$;


ALTER FUNCTION public.get_all_by_ids(args text[]) OWNER TO cha;

--
-- Name: get_authors_arr(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_authors_arr(paper_record_id integer) RETURNS text[]
    LANGUAGE sql
    AS $$
SELECT authors_array
FROM authors_array
WHERE record_id=paper_record_id;
$$;


ALTER FUNCTION public.get_authors_arr(paper_record_id integer) OWNER TO postgres;

--
-- Name: get_papers_by_authors_last_name(text); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.get_papers_by_authors_last_name(last_name_in text) RETURNS TABLE(paper_record_id integer, title text, subtitle text, journal_title text, pubdate date, volume text, pages text, doi text, doc_type text, get_authors_arr text[])
    LANGUAGE plpgsql
    AS $$
DECLARE
ids INT[] :=  (SELECT ARRAY_AGG(ra.record_id) as ids
      FROM records_to_authors ra, authors a
      WHERE a.last_name = last_name_in AND
      ra.authors_id = a.id);
BEGIN
RETURN QUERY SELECT p.*
FROM papers_biblio as p
WHERE p.paper_record_id = ANY(ids);
END;
$$;


ALTER FUNCTION public.get_papers_by_authors_last_name(last_name_in text) OWNER TO cha;

--
-- Name: insert_and_return_ident(text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insert_and_return_ident(paper text[]) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$
DECLARE
	paper_id INT;
	journal_id INT:=0;
BEGIN

INSERT INTO paper_info
	(title, subtitle, pubdate, fingerprint)
VALUES
	(paper[2],paper[3],TO_DATE(paper[5],'YYYY-MM-DD'), MD5(paper[1])::uuid)
ON CONFLICT DO NOTHING;

SELECT id INTO paper_id FROM paper_info WHERE fingerprint=MD5(paper[1])::UUID;
RETURN ARRAY[paper_id,journal_id];
END;$$;


ALTER FUNCTION public.insert_and_return_ident(paper text[]) OWNER TO postgres;

--
-- Name: insert_authors(json, integer); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.insert_authors(auth_doc json, record_id integer) RETURNS void
    LANGUAGE sql
    AS $$
  WITH
  array_auts AS
  (
  SELECT json_array_elements(auth_doc) as aut
  ),
  old_aut AS
  (
	SELECT id
	FROM authors
	WHERE orcid in
	      (
	      SELECT aut->>'orcid' FROM array_auts
	      )
  ),
  aut_insert AS
  (
       INSERT INTO authors
       	  (full_name, last_name, first_name, orcid)
       SELECT a.aut->>'full_name', a.aut->>'last_name', a.aut->>'first_name', a.aut->>'orcid'
       FROM
       array_auts a
       ON CONFLICT (orcid) DO NOTHING
       RETURNING id
 )
 INSERT INTO records_to_authors
 (SELECT record_id, id FROM old_aut) UNION (SELECT record_id, id FROM aut_insert);
$$;


ALTER FUNCTION public.insert_authors(auth_doc json, record_id integer) OWNER TO cha;

--
-- Name: raw_clean(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.raw_clean(doc text, source text) RETURNS text
    LANGUAGE plpython3u
    AS $$
  import sys, json
  sys.path.append("/home/cha")
  from dissemin_python_postgres import parse_info
  try:
  	return parse_info(doc, source)
  except Exception as E:
        return json.dumps({'error':str(E), 'type':str(type(E))})
$$;


ALTER FUNCTION public.raw_clean(doc text, source text) OWNER TO postgres;

--
-- Name: raw_insert(); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.raw_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	paper JSON := (SELECT raw_clean(new.doc, new.source::text))::JSON;
	keywords TEXT[] := (SELECT json_array_elements_text(paper->'keywords'));
	keywords_int INT[];
	paper_fingerprint UUID:= MD5(paper->>'fingerprint')::uuid;
	paper_identity INT;-- := add_paper(paper, paper_fingerprint);
	publisher_name TEXT := paper->'journal'->>'publisher';
	publisher_identity INT;
	journal_identity INT;
	nrecord_id INT;
BEGIN
	--- If publisher is not null then add and/or return its id
	IF (publisher_name IS NOT NULL) THEN
		SELECT add_publisher(publisher_name) INTO publisher_identity;
	END IF;
	--- If journal title is not null then add and/or return its id
	IF (paper->'journal'->>'title' IS NOT NULL) THEN
		SELECT add_journal(paper->'journal', publisher_identity) INTO journal_identity;
	END IF;
	--- If some keywords are presents, then add them and returns their id's
	IF (EXISTS(SELECT UNNEST(keywords))) THEN
	   SELECT add_kw(keywords) into keywords_int;
	END IF;
	--- If the python's script return an error json, we don't pursue.
	IF (paper::JSONB ? 'error') THEN
		NEW.error = paper;
	ELSE
	BEGIN
	--- Check if the fingerprint already appears add if not and returns the id in each cases.
	WITH paper_exists AS (
	         SELECT id FROM papers_info WHERE papers_info.fingerprint = paper_fingerprint
	     ),
	     paper_insert AS (
		  INSERT INTO
		      papers_info (title, subtitle, pubdate, fingerprint)
		  VALUES
		  (
		  paper->>'title',
		  paper->>'subtitle',
		  TO_DATE(paper->>'pubdate','YYYY-MM-DD'),
		  paper_fingerprint
		  )
		  ON CONFLICT (fingerprint) DO NOTHING
		  RETURNING id
	    )
        SELECT id into paper_identity
	FROM (SELECT * FROM paper_exists UNION SELECT * FROM paper_insert) s;

	--- Insert a new papers_records
	INSERT INTO papers_records
	(fingerprint, doi, splash_url, url, description,doc_type, keywords, pubdate,raw_id, paper_id, journal_id, volume, pages)
	VALUES
	(
	 paper_fingerprint,
	 paper->>'doi',
	 paper->>'splash_url',
	 paper->>'pdf_url',
	 paper->>'description',
	 (paper->>'doc_type')::doctype,
	 keywords_int,
	 TO_DATE(paper->>'pubdate', 'YYYY-MM-DD'),
	 new.id,
	 paper_identity,
	 journal_identity,
	 paper->>'volume',
	 paper->>'pages'
	 )
	RETURNING id INTO nrecord_id;
	new.record_id := nrecord_id;
	--- INSERT authors in author table with the record_id.
	PERFORM insert_authors(paper->'authors', nrecord_id);
	END;
	END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.raw_insert() OWNER TO cha;

--
-- Name: truncate_all_tables(); Type: FUNCTION; Schema: public; Owner: cha
--

CREATE FUNCTION public.truncate_all_tables() RETURNS void
    LANGUAGE sql
    AS $$TRUNCATE TABLE affiliation, authors, journals, journals_identifier, journals_serials, keywords, papers_info, papers_records, publishers, raw
CASCADE$$;


ALTER FUNCTION public.truncate_all_tables() OWNER TO cha;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: papers_records; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.papers_records (
    id integer NOT NULL,
    fingerprint uuid,
    splash_url text,
    url text,
    description text,
    keywords integer[],
    pubdate date,
    raw_id integer,
    paper_id integer,
    journal_id integer,
    volume text,
    pages text,
    doi text,
    doc_type public.doctype
);


ALTER TABLE public.papers_records OWNER TO postgres;

--
-- Name: _agg_records_doc_type; Type: MATERIALIZED VIEW; Schema: public; Owner: cha
--

CREATE MATERIALIZED VIEW public._agg_records_doc_type AS
 SELECT papers_records.doc_type,
    array_agg(papers_records.id) AS ids
   FROM public.papers_records
  GROUP BY papers_records.doc_type
  WITH NO DATA;


ALTER TABLE public._agg_records_doc_type OWNER TO cha;

--
-- Name: affiliation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.affiliation (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.affiliation OWNER TO postgres;

--
-- Name: affiliation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.affiliation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliation_id_seq OWNER TO postgres;

--
-- Name: affiliation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.affiliation_id_seq OWNED BY public.affiliation.id;


--
-- Name: authors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authors (
    id integer NOT NULL,
    full_name text,
    first_name text,
    last_name text,
    orcid text
);


ALTER TABLE public.authors OWNER TO postgres;

--
-- Name: records_to_authors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.records_to_authors (
    record_id integer,
    authors_id integer
);


ALTER TABLE public.records_to_authors OWNER TO postgres;

--
-- Name: authors_array; Type: VIEW; Schema: public; Owner: cha
--

CREATE VIEW public.authors_array AS
 SELECT pr.id AS record_id,
    array_agg(a.full_name) AS authors_array
   FROM public.papers_records pr,
    public.records_to_authors ra,
    public.authors a
  WHERE ((pr.id = ra.record_id) AND (ra.authors_id = a.id))
  GROUP BY pr.id;


ALTER TABLE public.authors_array OWNER TO cha;

--
-- Name: authors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authors_id_seq OWNER TO postgres;

--
-- Name: authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.authors_id_seq OWNED BY public.authors.id;


--
-- Name: journals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.journals (
    id integer NOT NULL,
    title text,
    publisher_id integer
);


ALTER TABLE public.journals OWNER TO postgres;

--
-- Name: journals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.journals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.journals_id_seq OWNER TO postgres;

--
-- Name: journals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.journals_id_seq OWNED BY public.journals.id;


--
-- Name: journals_identifier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.journals_identifier (
    journal_id integer,
    serial public.serial_number
);


ALTER TABLE public.journals_identifier OWNER TO postgres;

--
-- Name: journals_serials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.journals_serials (
    journal_id integer,
    serial_number public.serial_number
);


ALTER TABLE public.journals_serials OWNER TO postgres;

--
-- Name: keywords; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.keywords (
    id integer NOT NULL,
    keyword text
);


ALTER TABLE public.keywords OWNER TO postgres;

--
-- Name: keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keywords_id_seq OWNER TO postgres;

--
-- Name: keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.keywords_id_seq OWNED BY public.keywords.id;


--
-- Name: papers_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.papers_info (
    id integer NOT NULL,
    fingerprint uuid,
    title text,
    subtitle text,
    pubdate date
);


ALTER TABLE public.papers_info OWNER TO postgres;

--
-- Name: papers_biblio; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.papers_biblio AS
 SELECT pr.id AS paper_record_id,
    pi.title,
    pi.subtitle,
    j.title AS journal_title,
    pr.pubdate,
    pr.volume,
    pr.pages,
    pr.doi,
    pr.doc_type,
    ( SELECT public.get_authors_arr(pr.id) AS get_authors_arr) AS get_authors_arr
   FROM public.papers_info pi,
    public.journals j,
    public.papers_records pr
  WHERE ((j.id = pr.journal_id) AND (pi.id = pr.paper_id));


ALTER TABLE public.papers_biblio OWNER TO postgres;

--
-- Name: papers_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.papers_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.papers_info_id_seq OWNER TO postgres;

--
-- Name: papers_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.papers_info_id_seq OWNED BY public.papers_info.id;


--
-- Name: papers_records_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.papers_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.papers_records_id_seq OWNER TO postgres;

--
-- Name: papers_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.papers_records_id_seq OWNED BY public.papers_records.id;


--
-- Name: publishers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.publishers (
    id integer NOT NULL,
    romeo_id integer,
    name text,
    alias text[],
    url text,
    preprint public.publication_status,
    postprint public.publication_status,
    pdfversion public.publication_status,
    oa_status public.oa_status,
    last_updated timestamp without time zone DEFAULT now(),
    romeo_parent_id integer
);


ALTER TABLE public.publishers OWNER TO postgres;

--
-- Name: publishers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.publishers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publishers_id_seq OWNER TO postgres;

--
-- Name: publishers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.publishers_id_seq OWNED BY public.publishers.id;


--
-- Name: raw; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.raw (
    id integer NOT NULL,
    doc text,
    source public.source_doc,
    error text DEFAULT ''::text,
    record_id integer DEFAULT '-1'::integer
);


ALTER TABLE public.raw OWNER TO postgres;

--
-- Name: raw_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.raw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_id_seq OWNER TO postgres;

--
-- Name: raw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.raw_id_seq OWNED BY public.raw.id;


--
-- Name: affiliation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.affiliation ALTER COLUMN id SET DEFAULT nextval('public.affiliation_id_seq'::regclass);


--
-- Name: authors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authors ALTER COLUMN id SET DEFAULT nextval('public.authors_id_seq'::regclass);


--
-- Name: journals id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals ALTER COLUMN id SET DEFAULT nextval('public.journals_id_seq'::regclass);


--
-- Name: keywords id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keywords ALTER COLUMN id SET DEFAULT nextval('public.keywords_id_seq'::regclass);


--
-- Name: papers_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_info ALTER COLUMN id SET DEFAULT nextval('public.papers_info_id_seq'::regclass);


--
-- Name: papers_records id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_records ALTER COLUMN id SET DEFAULT nextval('public.papers_records_id_seq'::regclass);


--
-- Name: publishers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publishers ALTER COLUMN id SET DEFAULT nextval('public.publishers_id_seq'::regclass);


--
-- Name: raw id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.raw ALTER COLUMN id SET DEFAULT nextval('public.raw_id_seq'::regclass);


--
-- Name: affiliation affiliation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.affiliation
    ADD CONSTRAINT affiliation_pkey PRIMARY KEY (id);


--
-- Name: authors authors_orcid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_orcid_key UNIQUE (orcid);


--
-- Name: authors authors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);


--
-- Name: journals_identifier journals_identifier_serial_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals_identifier
    ADD CONSTRAINT journals_identifier_serial_key UNIQUE (serial);


--
-- Name: journals journals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals
    ADD CONSTRAINT journals_pkey PRIMARY KEY (id);


--
-- Name: journals_serials journals_serials_serial_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals_serials
    ADD CONSTRAINT journals_serials_serial_number_key UNIQUE (serial_number);


--
-- Name: keywords keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keywords
    ADD CONSTRAINT keywords_pkey PRIMARY KEY (id);


--
-- Name: papers_info papers_info_fingerprint_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_info
    ADD CONSTRAINT papers_info_fingerprint_key UNIQUE (fingerprint);


--
-- Name: papers_info papers_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_info
    ADD CONSTRAINT papers_info_pkey PRIMARY KEY (id);


--
-- Name: papers_records papers_records_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_records
    ADD CONSTRAINT papers_records_pkey PRIMARY KEY (id);


--
-- Name: publishers publishers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publishers
    ADD CONSTRAINT publishers_pkey PRIMARY KEY (id);


--
-- Name: publishers publishers_romeo_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publishers
    ADD CONSTRAINT publishers_romeo_id_key UNIQUE (romeo_id);


--
-- Name: raw raw_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.raw
    ADD CONSTRAINT raw_pkey PRIMARY KEY (id);


--
-- Name: keywords uniq_kw; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keywords
    ADD CONSTRAINT uniq_kw UNIQUE (keyword);


--
-- Name: authors_first_name_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authors_first_name_id_idx ON public.authors USING btree (first_name, id);


--
-- Name: authors_first_name_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authors_first_name_idx ON public.authors USING btree (first_name);


--
-- Name: authors_last_name_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authors_last_name_idx ON public.authors USING btree (last_name);


--
-- Name: fts_raw_doc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fts_raw_doc ON public.raw USING gin (to_tsvector('english'::regconfig, doc));


--
-- Name: fts_title; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fts_title ON public.papers_info USING gin (to_tsvector('english'::regconfig, title));


--
-- Name: journals_identifier_journal_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX journals_identifier_journal_id_idx ON public.journals_identifier USING btree (journal_id);


--
-- Name: journals_identifier_serial_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX journals_identifier_serial_idx ON public.journals_identifier USING btree (serial);


--
-- Name: papers_info_fingerprint_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_info_fingerprint_idx ON public.papers_info USING btree (fingerprint);


--
-- Name: papers_records_doc_type_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_doc_type_idx ON public.papers_records USING btree (doc_type);


--
-- Name: papers_records_fingerprint_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_fingerprint_idx ON public.papers_records USING btree (fingerprint);


--
-- Name: papers_records_id_doc_type_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_id_doc_type_idx ON public.papers_records USING btree (id, doc_type);


--
-- Name: papers_records_journal_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_journal_id_idx ON public.papers_records USING btree (journal_id);


--
-- Name: papers_records_paper_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_paper_id_idx ON public.papers_records USING btree (paper_id);


--
-- Name: papers_records_raw_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX papers_records_raw_id_idx ON public.papers_records USING btree (raw_id);


--
-- Name: raw_records_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX raw_records_id_idx ON public.raw USING btree (record_id);


--
-- Name: records_to_authors_authors_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX records_to_authors_authors_id_idx ON public.records_to_authors USING btree (authors_id);


--
-- Name: records_to_authors_authors_id_record_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX records_to_authors_authors_id_record_id_idx ON public.records_to_authors USING btree (authors_id, record_id);


--
-- Name: records_to_authors_record_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX records_to_authors_record_id_idx ON public.records_to_authors USING btree (record_id);


--
-- Name: unique_journal_title; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_journal_title ON public.journals USING btree (lower(title));


--
-- Name: unique_publishers_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_publishers_name ON public.publishers USING btree (lower(name));


--
-- Name: raw _raw_insert_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER _raw_insert_trigger BEFORE INSERT ON public.raw FOR EACH ROW EXECUTE PROCEDURE public.raw_insert();


--
-- Name: journals_identifier journal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals_identifier
    ADD CONSTRAINT journal_id_fkey FOREIGN KEY (journal_id) REFERENCES public.journals(id);


--
-- Name: journals journals_publisher_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.journals
    ADD CONSTRAINT journals_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES public.publishers(id);


--
-- Name: papers_records papers_records_journal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_records
    ADD CONSTRAINT papers_records_journal_id_fkey FOREIGN KEY (journal_id) REFERENCES public.journals(id);


--
-- Name: papers_records papers_records_paper_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.papers_records
    ADD CONSTRAINT papers_records_paper_id_fkey FOREIGN KEY (paper_id) REFERENCES public.papers_info(id);


--
-- Name: publishers publishers_romeo_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publishers
    ADD CONSTRAINT publishers_romeo_parent_id_fkey FOREIGN KEY (romeo_parent_id) REFERENCES public.publishers(romeo_id);


--
-- Name: records_to_authors records_to_author_authors_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records_to_authors
    ADD CONSTRAINT records_to_author_authors_id_fkey FOREIGN KEY (authors_id) REFERENCES public.authors(id);


--
-- Name: records_to_authors records_to_author_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records_to_authors
    ADD CONSTRAINT records_to_author_record_id_fkey FOREIGN KEY (record_id) REFERENCES public.papers_records(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA public TO cha;


--
-- Name: TABLE papers_records; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.papers_records TO cha;


--
-- Name: TABLE affiliation; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.affiliation TO cha;


--
-- Name: SEQUENCE affiliation_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.affiliation_id_seq TO cha;


--
-- Name: TABLE authors; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.authors TO cha;


--
-- Name: TABLE records_to_authors; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.records_to_authors TO cha;


--
-- Name: SEQUENCE authors_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.authors_id_seq TO cha;


--
-- Name: TABLE journals; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.journals TO cha;


--
-- Name: SEQUENCE journals_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.journals_id_seq TO cha;


--
-- Name: TABLE journals_identifier; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.journals_identifier TO cha;


--
-- Name: TABLE journals_serials; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.journals_serials TO cha;


--
-- Name: TABLE keywords; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.keywords TO cha;


--
-- Name: SEQUENCE keywords_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.keywords_id_seq TO cha;


--
-- Name: TABLE papers_info; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.papers_info TO cha;


--
-- Name: SEQUENCE papers_info_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.papers_info_id_seq TO cha;


--
-- Name: SEQUENCE papers_records_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.papers_records_id_seq TO cha;


--
-- Name: TABLE publishers; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.publishers TO cha;


--
-- Name: SEQUENCE publishers_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.publishers_id_seq TO cha;


--
-- Name: TABLE raw; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.raw TO cha;


--
-- Name: SEQUENCE raw_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.raw_id_seq TO cha;


--
-- PostgreSQL database dump complete
--

